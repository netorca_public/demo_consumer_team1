# Demo Consumer Repository

This repository represents how a typical Consumer would manage their ServiceItems with NetOrca. 

This demo contains an example CI file for Gitlab that is able validate and build the ServiceItem requests through to NetOrca. 
Other CI/CD processes and GIT providers can be easily supported in this process. 

## Requirements

This repository requires that a NetOrca api key be generated and added as an environment variable with the following field name 'netorca_key'.

In addition the .netorca/config.yaml 'base_url' field needs to be configured with the url of the NetOrca instance for the organisation. 

## Applications

All requests for ServiceItems are contained within Applications. 
These applications can be created as seperate yaml files within the .netorca folder. 

## Change process

The typical change process for modification,  deletion of creation of ServiceItems with this repo would be as follows: 

1. Branch from 'main' created, ServiceItem configuration is added and pushed. 
2. Merge request created from the new branch to 'main'
3. The CI/CD process will validate all changes against the NetOrca database and if valid the build for that merge request will pass. 
4. The Merge request can be reviewed and approved by teams as required
5. Changes are merged, the main build process will submit the new Application/ServiceItem configuration through to NetOrca
6. If Modify/Create/Delete changes are detected to ServiceItems they will be raised as ChangeInstances for the respecitve ServiceOwners. 


## Using this repo in the demo

Anyone is able to create a new branch/merge request to this repository and test the NetOrca validation. 

Please contact someone from the NetOrca team if you would like you merge request approved and would like to see the change instances that are created. 


